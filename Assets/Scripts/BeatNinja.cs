﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BeatNinja : MonoBehaviour {
    AudioSource _audio;
    public AudioClip currentAudioclip;
    public float maxScale = 0f;
    public float lowerboundSpawn;
    public float lowerboundSpawn1;
    public Slider lowerBoundBassSlider;
    public Slider lowerBoundTrebleSlider;
    public TextMeshProUGUI sliderBassText;
    public TextMeshProUGUI sliderTrebleText;
    public GameObject cubePrefab;
    public GameObject shootingCube;
    public AudioSource unMutedSource;
    public AudioSource mutedSource;
    public Button startButton;
    public Button exitButton;
    public Button resetButton;
    public Button songListButton;
    public GameObject songChooserPanel;
    public float songDelay = 0;
    public float delayCubeSpawn = 0.5f;

    [Header ("Particles")]
    public ParticleSystem starObj;
    public ParticleSystem burstParticle;
    public ParticleSystem burstParticleRed;
    public Material matLeft;
    public Material matRight;
    public Color colorLeft;
    public Color colorRight;

    GameObject[] sampleCube = new GameObject[512];
    public float[] _samples = new float[512];
    public float[] _samplesMute = new float[512];
    public float[] _freqband = new float[8];

    private float updateCounter = 0f;

    void Start () {
        //_audio = GetComponent<AudioSource> ();
        InstantiateTheCubes ();
        startButton.onClick.AddListener (delegate { lowerboundSpawn = lowerBoundBassSlider.value; lowerboundSpawn1 = lowerBoundTrebleSlider.value; StartDelayedSong (); startButton.gameObject.SetActive (false); });
        exitButton.onClick.AddListener (delegate { Application.Quit (0); });
        resetButton.onClick.AddListener (delegate { SceneManager.LoadScene ("GameScene"); });
        songListButton.onClick.AddListener (delegate { songChooserPanel.SetActive (true); });
        lowerBoundBassSlider.onValueChanged.AddListener (delegate { sliderBassText.text = lowerBoundBassSlider.value.ToString ("0.00"); });
        lowerBoundTrebleSlider.onValueChanged.AddListener (delegate { sliderTrebleText.text = lowerBoundTrebleSlider.value.ToString ("0.00"); });
        mutedSource.clip = currentAudioclip;
        unMutedSource.clip = currentAudioclip;
    }

    // Update is called once per frame
    void Update () {
        GetSpectrumAudioSourceUnmuted ();
        GetSpectrumAudioSourceMuted ();
        if (updateCounter > delayCubeSpawn) {
            MakeFequencybands ();
        }
        updateCounter += Time.deltaTime;

    }

    void GetSpectrumAudioSourceUnmuted () {
        unMutedSource.GetSpectrumData (_samples, 0, FFTWindow.Blackman);
        for (int i = 0; i < 512; i++) {
            if (sampleCube != null) {
                sampleCube[i].transform.localScale = new Vector3 (1, 1, _samples[i] * maxScale);
            }
        }
    }
    void GetSpectrumAudioSourceMuted () {
        mutedSource.GetSpectrumData (_samplesMute, 0, FFTWindow.Blackman);
    }

    void InstantiateTheCubes () {
        for (int i = 0; i < 512; i++) {
            GameObject cube = (GameObject) Instantiate (cubePrefab);
            cube.transform.position = this.transform.position;
            cube.transform.parent = this.transform;
            cube.name = "cube" + i;
            this.transform.eulerAngles = new Vector3 (0, -0.703125f * i, 0);
            cube.transform.position = Vector3.forward * 100;
            sampleCube[i] = cube;
        }
    }

    void SpawnCubeStraightLine (bool leftIsBass) {
        updateCounter = 0f;
        float randHeight = Random.Range (-7f, -11f);
        Vector3 leftPos = new Vector3 (-3f, 0f, randHeight);
        Vector3 rightPos = new Vector3 (3f, 0f, randHeight);
        Vector3 currentPos = new Vector3 (0, 0, 0);
        bool isleft = false;
        // if (Random.Range (0f, 1f) > 0.50f) {
        if (leftIsBass) {
            currentPos = leftPos;
            isleft = true;
        } else {
            currentPos = rightPos;
            isleft = false;
        }

        GameObject tempObj = Instantiate (shootingCube, currentPos, Quaternion.identity);
        TrailRenderer tr = tempObj.GetComponent<TrailRenderer> ();
        float alpha = 1.0f;
        Gradient gradient = new Gradient ();
        tempObj.transform.GetComponent<Rigidbody> ().AddForce (Vector3.up * 100, ForceMode.VelocityChange);
        if (isleft) {
            tempObj.transform.GetComponent<MeshRenderer> ().material = matLeft;
            gradient.SetKeys (
                new GradientColorKey[] { new GradientColorKey (colorLeft, 0.0f), new GradientColorKey (Color.white, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey (alpha, 0.0f), new GradientAlphaKey (alpha, 1.0f) }
            );
            burstParticle.Play ();
        } else {
            tempObj.transform.GetComponent<MeshRenderer> ().material = matRight;
            gradient.SetKeys (
                new GradientColorKey[] { new GradientColorKey (colorRight, 0.0f), new GradientColorKey (Color.white, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey (alpha, 0.0f), new GradientAlphaKey (alpha, 1.0f) }
            );
            burstParticleRed.Play ();
        }
        tr.colorGradient = gradient;
    }

    void StartDelayedSong () {
        StartCoroutine (DelayedSongRoutine ());
    }

    IEnumerator DelayedSongRoutine () {
        mutedSource.Play ();
        yield return new WaitForSeconds (songDelay);
        unMutedSource.Play ();
    }
    void MakeFequencybands () {
        //
        int count = 0;
        for (int i = 0; i < 8; i++) {
            float average = 0;
            int sampleCount = (int) Mathf.Pow (2, i) * 2;
            if (i == 7) {
                sampleCount += 2;
            }
            for (int j = 0; j < sampleCount; j++) {
                average += _samplesMute[count] * (count + 1);
                count++;
            }
            average /= count;
            _freqband[i] = average * 10;
        }
        //Debug.Log ("_freqband [0] : "+ _freqband [0]);
        if (_freqband[0] > lowerboundSpawn) {
                SpawnCubeStraightLine (true);
                StarObjDance (_samples[0]);
            
        }
        if (_freqband[1] > lowerboundSpawn1) {
            //Debug.Log ("Spawn cube here!");
            SpawnCubeStraightLine (false);
            StarObjDance (_samples[1]);
        }
    }

    void StarObjDance (float yValue) {
        //starObj.transform.localScale = new Vector3 (1+yValue * 3000f, 1+yValue * 3000f , 1+yValue * 3000f);
        // = new Vector3 (starObj.transform.position.x, yValue * 5000f , starObj.transform.position.z);
        if (Random.Range (0f, 1f) > 0.5f) {
            starObj.startSpeed = yValue * -3000f;
        } else {
            starObj.startSpeed = yValue * 3000f;
        }
        // if ((yValue * 3000f) > lowerboundSpawn) {
        //     burstParticle.Play ();
        // }
    }

    public void SetSelectedAudioClip (AudioClip clip) {
        currentAudioclip = clip;

        mutedSource.clip = currentAudioclip;
        unMutedSource.clip = currentAudioclip;
    }
}