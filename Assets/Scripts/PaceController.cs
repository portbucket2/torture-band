using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaceController : MonoBehaviour
{
    [SerializeField] AudioSource matchSource;
    [SerializeField] LineRenderer ln;
    [SerializeField] int size = 512;
    [SerializeField] float multiplayer;
    [SerializeField] PianoTile pianoTile;
    [SerializeField] float bitMin;
    [SerializeField] float bitMulti;
    [SerializeField] List<Instrument> instruments;

    [Header("Debug: ")]
    [SerializeField] float bitHighest;
    [SerializeField] float spectrumValue;
    [SerializeField] bool tileMoving = false;
    [SerializeField] float[] _samples;

    bool tileCalled = false;
    float dx;
    [SerializeField] Vector3[] positions;


    [Header("Audio: ")]
    [SerializeField] float bias;
    [SerializeField] float lowerBias;
    [SerializeField] float timeStep;
    [SerializeField] float timeToBeat;
    [SerializeField] float restSmoothTime;

    [SerializeField] float m_previousAudioValue;
    [SerializeField] float m_audioValue;
    [SerializeField] float m_timer;
    [SerializeField] bool m_isBeat = false;

    GameController gameController;

    private void Awake()
    {
        _samples = new float[size];
    }
    void Start()
    {
        gameController = GameController.GetController();
        Vector3 ss = ln.GetPosition(0);
        Vector3 ee = ln.GetPosition(1);
        Vector3 dir = ss - ee;
        float startX = ss.x; 
        float dx = dir.magnitude / size;
        Debug.Log("startX: " + startX);
        Debug.Log("Dir: " + dir);
        Debug.Log("length: " + dir.magnitude);
        Debug.Log("dx: " + dx);
        positions = new Vector3[size];

        positions[0] = new Vector3((float)(startX + (0f * dx)), ss.y, -4.5f);
        //positions[0] = ss;

        for (int i = 1; i < size; i++)
        {
            positions[i] = new Vector3((float)(startX + (i * dx)), ss.y, -4.5f);
            //positions[i] = ss + dir * (i * dx);
            //Debug.Log("value: " + dir * (i * dx));
        }

        ln.positionCount = size;
        ln.SetPositions(positions);
    }
    private void Update()
    {
        GetSpectrumData();
    }
    void OnBeat()
    {
        //Debug.Log("Beat ");
        m_timer = 0;
        m_isBeat = true;
        pianoTile.ProduceTile();
    }
    void GetSpectrumData()
    {
        matchSource.GetSpectrumData(_samples, 0, FFTWindow.Blackman);

        if (tileMoving)
        {
            if (_samples != null && _samples.Length > 0)
            {
                //bitHighest = 0;
                //for (int i = 0; i < size; i++)
                //{
                //    float temp = _samples[i] * bitMulti;
                //    if (temp > bitHighest)
                //    {
                //        bitHighest = temp;
                //    }
                //}
                //spectrumValue = bitHighest;
                spectrumValue = _samples[0] * bitMulti;
            }
            m_previousAudioValue = m_audioValue;
            m_audioValue = spectrumValue;


            if (m_audioValue > bias && !m_isBeat)
            {
                if (m_timer > timeStep)
                {
                    OnBeat();
                }
            }
            if (m_audioValue <= lowerBias && m_isBeat)
            {
                m_isBeat = false;
            }


            //if (m_previousAudioValue > bias && m_audioValue <= bias)
            //{
            //    if (m_timer > timeStep)
            //    {
            //        OnBeat();
            //    }
            //}

            //if (m_previousAudioValue <= bias && m_audioValue > bias)
            //{
            //    if (m_timer > timeStep)
            //    {
            //        OnBeat();
            //    }
            //}

            m_timer += Time.deltaTime;
        }

        //----------------show spectrum data----------
        for (int i = 0; i < size; i++)
        {
            positions[i] = new Vector3(positions[i].x, positions[i].y, -4.5f + (_samples[i] * multiplayer));
            float temp = _samples[i] * bitMulti;
            //if (temp> bitMin && !tileCalled) 
            //{
            //    tileCalled = true;
            //    pianoTile.ProduceTile();
            //}
            //if (temp > bitHighest)
            //{
            //    bitHighest = temp;
            //}
        }

        ln.SetPositions(positions);
        tileCalled = false;
    }
    public void SyncAudio()
    {
        int max = instruments.Count;
        for (int i = 0; i < max; i++)
        {
            instruments[i].SyncAudio(instruments[0].GetAudioSource());
        }
        int playingCount = 0;
        for (int i = 0; i < max; i++)
        {
            if (instruments[i].GetAudioSource().isPlaying)
                playingCount++;            
        }
        if (playingCount >= max)
        {
            // all instruments playing
            gameController.BringCrowd();
        }
    }
    public void SyncMuteAudio(AudioClip _clip)
    {
        matchSource.clip = _clip;
        matchSource.Play();
        tileMoving = true;
    }
    public void StopTileMovement()
    {
        tileMoving = false;
    }
}
