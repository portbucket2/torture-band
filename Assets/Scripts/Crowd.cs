using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Crowd : MonoBehaviour
{
    [SerializeField] Vector3 endPoint;
    [SerializeField] float ShakeOffset;
    [SerializeField] float transitionTime;

    ParticleSystem party;

    void Start()
    {
        party = GetComponent<ParticleSystem>();
        
        //BringCrowd();
    }

    public void BringCrowd()
    {
        party.Play();
        transform.DOMove(endPoint, transitionTime).SetEase(Ease.OutSine).OnComplete(Shake);
    }

    void Shake()
    {
        transform.DOKill();
        transform.DOPunchPosition(Vector3.up * ShakeOffset, transitionTime/2f).OnComplete(Shake);
    }
}
