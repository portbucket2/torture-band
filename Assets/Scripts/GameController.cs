using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }
    [Header("Values")]
    [SerializeField] int pointLimit = 10;

    [Header("Modules")]
    [SerializeField] InputController inputController;
    [SerializeField] UIController uiController;
    [SerializeField] PaceController paceController;
    [SerializeField] Locator locator;
    [SerializeField] PianoTile pianoTile;
    [SerializeField] Crowd crowd;

    public InputController GetInputController() { return inputController; }
    public UIController GetUIController() { return uiController; }
    public PaceController GetPaceController() { return paceController; }
    public Locator GetLocator() { return locator; }
    public PianoTile GetPianoTile() { return pianoTile; }

    private int totalTilesProduced = 0;

    void Start()
    {
        
    }

    public void CheckPoint(int point)
    {
        if (point >= pointLimit)
        {
            Debug.Log("levelComplete");
            paceController.StopTileMovement();
            uiController.TileButtonState(false);
            uiController.ShowLevelEnd();
            totalTilesProduced = pianoTile.GetTotalTilesProduced();
        }
    }
    public void BringCrowd()
    {
        crowd.BringCrowd();
        uiController.TileButtonState(true);
    }
}
