using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Tile : MonoBehaviour
{
    float travelTime;
    Vector3 startPos;
    Vector3 endPos;

    void Start()
    {
        
    }

    public void SetTile(Vector3 _startPos, Vector3 _endPos, float _time)
    {
        startPos = _startPos;
        endPos = _endPos;
        travelTime = _time;
    }

    public void Move()
    {
        transform.DOLocalMove(endPos, travelTime).SetEase(Ease.Linear).OnComplete(Completed);
    }

    void Completed()
    {
        transform.localPosition = startPos;
        transform.DOKill();
    }
}
