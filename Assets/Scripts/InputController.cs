using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InputController : MonoBehaviour
{
    [SerializeField] Camera mainCam;
    [SerializeField] Vector3 grabOffset;

    [Header("Debug: ")]
    [SerializeField] Transform grabbedNPC;

    readonly string NPC = "NPC";
    readonly string INSTRUMENT = "instrument";
    readonly string GROUND = "ground";

    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            MouseDown();
        }
        if (Input.GetMouseButton(0))
        {
            MouseDrag();
        }
        if (Input.GetMouseButtonUp(0))
        {
            MouseUp();
        }
    }
    private void MouseDown()
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100)) {

            if (hit.transform.CompareTag(NPC))
            {
                grabbedNPC = hit.transform;
                grabbedNPC.GetComponent<NPCBehavior>().Grab();
            }
        }

    }
    private void MouseDrag()
    {
        if (grabbedNPC)
        {
            Vector3 pos = Vector3.zero;
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.transform.CompareTag(GROUND))
                {
                    pos = hit.point;
                    Debug.DrawLine(ray.origin, hit.point, Color.green);
                }
            }

            grabbedNPC.DOMove(pos + grabOffset, 0.1f);
        }
        
    }
    private void MouseUp()
    {

        if (!grabbedNPC)
            return;


        grabbedNPC.GetComponent<NPCBehavior>().Drop();

        grabbedNPC = null;
    }
}
