using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] Button tileButton;
    [SerializeField] TextMeshProUGUI pointText;    
    [SerializeField] TextMeshProUGUI perfectText;    
    [SerializeField] Animator buttonAnimator;
    [SerializeField] Animator textAnimator;
    [SerializeField] GameObject levelEndPanel;
    [SerializeField] Button restartButton;

    [Header("debug: ")]
    [SerializeField] int points;
    [SerializeField] Locator locator;

    readonly string CORRECT = "correct";
    readonly string WRONG = "wrong";

    readonly string PERFECT = "perfect";
    readonly string GOOD = "good";
    readonly string OK = "ok";


    GameController gameController;
    void Start()
    {
        gameController = GameController.GetController();
        locator = gameController.GetLocator();
        points = 0;
        tileButton.onClick.AddListener(delegate
        {
            TileButtonPress();
        });
        restartButton.onClick.AddListener(delegate
        {
            RestartLevel();
        });
        TileButtonState(false);
    }

    void TileButtonPress()
    {
        locator.Detect();
    }
    void UpdatePointText()
    {
        pointText.text = "Points : " + points;
    }
    void RestartLevel()
    {
        SceneManager.LoadScene(0);
    }
    public void CorrctAnswer()
    {
        points++;
        buttonAnimator.SetTrigger(CORRECT);
        UpdatePointText();
        gameController.CheckPoint(points);
    }
    public void WrongAnswer()
    {
        buttonAnimator.SetTrigger(WRONG);
    }
    public void TileButtonState(bool isON)
    {
        tileButton.gameObject.SetActive(isON);
    }
    public void ShowLevelEnd()
    {
        levelEndPanel.SetActive(true);
    }
    public void TextAnimationStaus(float _dis)
    {
        if (_dis > 0.7f)
        {
            perfectText.text = "Perfect";
            textAnimator.SetTrigger(PERFECT);
        }else if (_dis < 0.7f && _dis >= 0.3f)
        {
            perfectText.text = "Good";
            textAnimator.SetTrigger(GOOD);
        }
        else if (_dis >= 0f && _dis < 0.3f)
        {
            perfectText.text = "OK";
            textAnimator.SetTrigger(OK);
        }
    }
}
