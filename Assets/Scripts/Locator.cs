using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Locator : MonoBehaviour
{
    [SerializeField] bool detecting = false;

    [Header("debug: ")]
    [SerializeField] UIController uiController;

    readonly string TILE = "tile";

    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
        uiController = gameController.GetUIController();
    }

    private void OnTriggerStay(Collider other)
    {
        if (detecting)
        {
            if (other.transform.CompareTag(TILE))
            {
                detecting = false;
                float dis = Vector3.Distance(transform.position, other.transform.position);
                //Debug.Log("tile found!! + distance: " + dis);
                uiController.CorrctAnswer();
                uiController.TextAnimationStaus(dis);
            }
            else
            {
                detecting = false;
                uiController.WrongAnswer();
            }
        }
    }

    
    private void OnTriggerExit(Collider other)
    {
        
    }

    public void Detect()
    {
        detecting = true;
    }
}
