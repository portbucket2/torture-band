using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Instrument : MonoBehaviour
{
    [SerializeField] bool isMatching = false;
    [SerializeField] Transform victimPosition;
    [SerializeField] AudioClip audioClip;
    [SerializeField] float muteDelay;

    [Header("Debug: ")]
    [SerializeField] Transform currentVictim;
    [SerializeField] AudioSource audioSource;
    [SerializeField] PaceController paceController;
    [SerializeField] float rotSpeed = 10f;

    bool spin = false;
    float rot = 0f;
    BoxCollider col;

    WaitForSeconds WAIT;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        col = GetComponent<BoxCollider>();
        paceController = transform.parent.GetComponent<PaceController>();

    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (spin && currentVictim)
        {
            currentVictim.eulerAngles = new Vector3(45f, rot, 0f);
            rot += rotSpeed;
        }
    }
    void StartTorture()
    {
        audioSource.clip = audioClip;
        audioSource.Play();
        paceController.SyncAudio();
        spin = true;
        if (isMatching)
        {
            StartCoroutine(MuteAudioStart());
        }
        
    }
    IEnumerator MuteAudioStart()
    {
        AudioClip cc = audioSource.clip;
        float ll = cc.length;
        if (ll > muteDelay)
            ll -= muteDelay;
        else
            ll = muteDelay;
        WAIT = new WaitForSeconds(ll);
        yield return WAIT;
        paceController.SyncMuteAudio(cc);

    }
    public void SyncAudio(AudioSource source)
    {
        audioSource.timeSamples = source.timeSamples;
    }
    public void InitiateTorture(Transform npc)
    {
        col.enabled = false;
        currentVictim = npc;
        currentVictim.DOMove(victimPosition.position, 0.5f).SetEase(Ease.OutSine).OnComplete(StartTorture);
        currentVictim.GetComponent<CapsuleCollider>().enabled = true;
    }
    public AudioSource GetAudioSource()
    {
        return audioSource;
    }
    public void StopPlaying()
    {
        audioSource.Stop();
        spin = false;
        currentVictim = null;
        col.enabled = true;
    }

}
