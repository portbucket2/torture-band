using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCBehavior : MonoBehaviour
{
    [Header("Debug")]
    [SerializeField] Instrument currentInstrument;
    [SerializeField] bool captured = false;

    Rigidbody rb;
    CapsuleCollider col;
    NavMeshAgent agent;
    WanderingAI wanderingAI;

    

    readonly string INSTRUMENT = "instrument";
    readonly string GROUND = "ground";

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        agent = GetComponent<NavMeshAgent>();
        wanderingAI = GetComponent<WanderingAI>();
    }

    void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(INSTRUMENT))
        {
            currentInstrument = other.transform.GetComponent<Instrument>();
            Debug.Log("collided");
            Captured();
        }
        else
        {
            Drop();
        }
        
    }

    void Captured()
    {
        if (currentInstrument)
        {
            Debug.Log("captured");
            Grab();
            currentInstrument.InitiateTorture(transform);
            captured = true;
        }
    }
    void StatusCheck()
    {
        if (captured)
        {
            captured = false;
            currentInstrument.StopPlaying();
            currentInstrument = null;
        }
    }
    public void Grab()
    {
        rb.isKinematic = true;
        col.enabled = false;
        transform.localEulerAngles = Vector3.zero;
        agent.enabled = false;
        wanderingAI.WanderAI(false);
        StatusCheck();
    }
    public void Drop()
    {
        Debug.Log("dropped");
        rb.isKinematic = false;
        col.enabled = true;
        agent.enabled = true;
        wanderingAI.WanderAI(true);
    }
}
