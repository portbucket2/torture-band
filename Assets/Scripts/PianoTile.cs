using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PianoTile : MonoBehaviour
{
    [SerializeField] GameObject tilePrefab;
    [SerializeField] int preFabricationCount;
    [SerializeField] Transform locator;
    [SerializeField] Vector2 horizontalLimit;
    [SerializeField] float travelTime;

    [Header("Debug :")]
    [SerializeField] int tileCount = 0;
    [SerializeField] int totalTilesProduced = 0;
    //[SerializeField] int tileCountPrevious = 0;
    [SerializeField] Vector3 startPosition;
    [SerializeField] Vector3 endPosition;
    [SerializeField] List<Tile> tileList;
    void Start()
    {
        tileList = new List<Tile>();
        startPosition = new Vector3(horizontalLimit.x, 1, locator.position.z);
        endPosition = new Vector3(horizontalLimit.y, 1, locator.position.z);
        for (int i = 0; i < preFabricationCount; i++)
        {
            GameObject gg = Instantiate(tilePrefab,transform);
            Tile tt = gg.GetComponent<Tile>();
            tt.SetTile(startPosition, endPosition, travelTime);
            tileList.Add(tt);
        }
        //ProduceTile();
    }

    public void ProduceTile()
    {
        tileList[tileCount].Move();
        //tileCountPrevious = tileCount;
        tileCount++;
        totalTilesProduced++;
        if (tileCount >= preFabricationCount)
        {
            tileCount = 0;
        }
    }
    public int GetTotalTilesProduced()
    {
        return totalTilesProduced;
    }
}
